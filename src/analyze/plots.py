import pandas as pd
import numpy as np
import os,sys

from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import mannwhitneyu
import scipy.stats as stats

from scipy.optimize import curve_fit
from sklearn.linear_model import LinearRegression

from importlib import reload
import matplotlib.pyplot as plt
from matplotlib_venn import venn3, venn2
import seaborn as sns
import argparse


def venn(perid_above2):
    conditions1 = [(perid_above2['MMscore'] < 0.75),(perid_above2['MMscore'] >= 0.75)]

    conditions2 = [(perid_above2['min $DockQ_{i}$'] < 0.23),(perid_above2['min $DockQ_{i}$'] >= 0.23)]

    conditions3 = [(perid_above2['min $DockQ_{ij}$'] < 0.23),(perid_above2['min $DockQ_{ij}$'] >= 0.23)]

    values = [0,1]

    perid_above2['Venn_MM'] = np.select(conditions1, values)
    perid_above2['Venn_miDockQ'] = np.select(conditions2, values)
    perid_above2['Venn_pwDockQ'] = np.select(conditions3, values)

    v1 = len(perid_above2[(perid_above2['Venn_MM']==1) & (perid_above2['Venn_miDockQ']==0) & (perid_above2['Venn_pwDockQ']==0)])
    v2 = len(perid_above2[(perid_above2['Venn_MM']==0) & (perid_above2['Venn_miDockQ']==1) & (perid_above2['Venn_pwDockQ']==0)])
    v3 = len(perid_above2[(perid_above2['Venn_MM']==1) & (perid_above2['Venn_miDockQ']==1) & (perid_above2['Venn_pwDockQ']==0)])
    v4 = len(perid_above2[(perid_above2['Venn_MM']==0) & (perid_above2['Venn_miDockQ']==0) & (perid_above2['Venn_pwDockQ']==1)])
    v5 = len(perid_above2[(perid_above2['Venn_MM']==1) & (perid_above2['Venn_miDockQ']==0) & (perid_above2['Venn_pwDockQ']==1)])
    v6 = len(perid_above2[(perid_above2['Venn_MM']==0) & (perid_above2['Venn_miDockQ']==1) & (perid_above2['Venn_pwDockQ']==1)])
    v7 = len(perid_above2[(perid_above2['Venn_MM']==1) & (perid_above2['Venn_miDockQ']==1) & (perid_above2['Venn_pwDockQ']==1)])

    set1 = (perid_above2['Venn_MM'])
    set2 = (perid_above2['Venn_miDockQ'])
    set3 = (perid_above2['Venn_pwDockQ'])

    venn3(subsets=(v1, v2, v3, v4, v5, v6, v7), set_labels= ('MMscore', '$DockQ_{i}$', '$DockQ_{ij}$'))
    plt.savefig('figures/venn.jpg', dpi=350)

def corrfunc(x, y, ax=None, **kws):
    r, _ = spearmanr(x, y)
    label = r'$\rho$='+ str(round(r, 2))
    ax = plt.gca()
    #pos = (.6, .1 - .1*n)
    ax.set_aspect('equal', adjustable='box')
    pos = (0.1,1.1) #if kws['label'] == 'homomer' else (0.05,0.95)
    ax.annotate("Homomer:{}".format(label), xy = pos, size = 12, xycoords = ax.transAxes)

#dfm_select = perid_above2[['min $DockQ_{i}$', 'max $DockQ_{i}$', 'min $DockQ_{ij}$', 'max $DockQ_{ij}$', 'MMscore','class']]
dfm_select = perid_above2[['min $DockQ_{i}$', 'max $DockQ_{i}$', 'MM-score','class']]
dfm_select1 = dfm_select[dfm_select["class"]=='homomer']

g=sns.pairplot(data=dfm_select1, corner=True)
#g=sns.pairplot(data=dfm_select, hue='class', corner=True)
g.map_lower(corrfunc)

axis_font = {'size':'12'}

plt.subplots_adjust(wspace =0.2,hspace =0.2)
g.set(ylim=(0,1))
g.set(xlim=(0,1))
plt.yticks(fontsize=12)
plt.xticks(fontsize=12)
handles = g._legend_data.values()
labels = g._legend_data.keys()
#g.fig.legend(handles=handles, labels=labels, bbox_to_anchor=(0.85, 0.5))
#g._legend.remove()

#plt.savefig('figures/dockqi_homo_pairplot_1-both.jpg',dpi=450)
plt.show()


def successful_interfaces(perchain):
    perchain=perchain.drop_duplicates(subset = ['pdbid', '$DockQ_{i}$','chain1'],keep = 'last').reset_index(drop = True)
    succdockq_IFcount=perchain.groupby('pdbid').apply(lambda grp: len(grp[grp['$DockQ_{i}$']>=0.23])).reset_index()
    succdockq_IFcount.rename(columns={0:'No. of correct interfaces'},inplace=True)
    perchain=perchain.merge(succdockq_IFcount,on='pdbid')

    plt.figure(figsize=(12, 8))
    ax1 = plt.subplot2grid(shape=(2,6), loc=(0,0), colspan=2)
    ax2 = plt.subplot2grid((2,6), (0,2), colspan=2)
    ax3 = plt.subplot2grid((2,6), (0,4), colspan=2)
    ax4 = plt.subplot2grid((2,6), (1,1), colspan=2)
    ax5 = plt.subplot2grid((2,6), (1,3), colspan=2)

    for num, axis in zip([2,3,4,5,6],[ax1,ax2,ax3,ax4,ax5]):
        sns.countplot(data=perchain[perchain['num_chains']==num],x='No. of correct interfaces',hue='class',
                  hue_order=['homomer','heteromer'],ax=axis)
        plt.yticks(fontsize=12)
        plt.xticks(fontsize=12)
        plt.xlabel("No. of correct interfaces")
        axis.legend([],[], frameon=False)

    plt.subplots_adjust(wspace=1.2, right=0.9)
    plt.tight_layout()
    plt.yticks(fontsize=12)
    plt.xticks(fontsize=12)
    plt.savefig('figures/correct_interfaces.jpg',dpi=350)

def pdockq_i(df):

    # Optimizing i
    for i in [1,2,3,5,10,15,20,25,30,40]:
        df["PAE_inter_norm"]=1/(1+(df['PAE_inter']/i)**2)
        df["IF_interPAE_norm"]=1/(1+(df['IF_interPAE']/i)**2)
        print (i,np.round(np.corrcoef(df.DockQ,df.PAE_inter_norm,)[0,1],3),np.round(np.corrcoef(df.DockQ,df.IF_interPAE_norm)[0,1],3),
               i,np.round(stats.spearmanr(df.DockQ,df.PAE_inter_norm)[0],3),round(stats.spearmanr(df.DockQ,df.IF_interPAE_norm)[0],3))

        i=10
        df["PAE_inter_norm"]=1/(1+(df['PAE_inter']/i)**2)
        df["IF_interPAE_norm"]=1/(1+(df['IF_interPAE']/i)**2)

    def func(x, a, b, c):
        return a * np.exp(-b * x) + c
    def func(x, a, b,c):
        return x[0] * a+b * x[1]+ c
    def sigmoid(x, L ,x0, k, b):
            y = L / (1 + np.exp(-k*(x-x0)))+b
            return (y)

    def fit_newscore(column):
         testdf = df[df[column]>0]
         colval = testdf[column].values
         dockq = testdf.DockQ.values
         xdata =colval[np.argsort(colval)]
         ydata = dockq[np.argsort(dockq)]

         p0 = [max(ydata), np.median(xdata),1,min(ydata)] # this is an mandatory initial guess
         popt, pcov = curve_fit(sigmoid, xdata, ydata,p0)# method='dogbox', maxfev=50000)
    
         tiny=1.e-20
         print('L=',np.round(popt[0],3),'x0=',np.round(popt[1],3), 'k=',np.round(popt[2],3), 'b=',np.round(popt[3],3))

         x_pmiDockQ = testdf[column].values
         x_pmiDockQ = x_pmiDockQ[np.argsort(x_pmiDockQ)]
         y_pmiDockQ = sigmoid(x_pmiDockQ, *popt)
         print("Average error for sigmoid fit is ", np.average(np.absolute(y_pmiDockQ-ydata)))

         #sns.kdeplot(data=df,x=column,y='DockQ',kde=True,levels=5,fill=True, alpha=0.8, cut=0)
         sns.scatterplot(data=df,x=column,y='DockQ', hue='class')
         plt.legend([],[], frameon=False)
    
         plt.plot(x_pmiDockQ, y_pmiDockQ,label='fit',color='k',linewidth=2)
         return popt

    ## Linear regression
    linear_regressor = LinearRegression()
    #df["mult"]=df.IF_interPAE_norm*df.IFplDDT_afm/100
    df["mult"]=df.IF_interPAE_norm*df.IFplDDT_afm

    X = df[["mult"]]
    Y = df["DockQ"]

    reg=linear_regressor.fit(X, Y) 
    Y_pred = linear_regressor.predict(X)

    print (np.round(np.corrcoef(Y,Y_pred)[0,1],3), round(stats.spearmanr(Y,Y_pred)[0],3))
    print(reg.score(X, Y),reg.coef_)
    plt.scatter(Y_pred,Y)
    plt.show()

    ## Sigmoid fit
    df_test=df.copy()
    fitpopt=fit_newscore('mult')
    df_test['pmiDockQ_mult']=sigmoid(df_test.mult.values,*fitpopt)
    plt.xlabel('Normalised interface PAE * interface plDDT')
    plt.ylabel('$DockQ_{i}$')
    plt.tight_layout()

    plt.savefig('figures/pdockq_1.jpg',format='jpg',dpi=350)

def pairplot_2(perid_above2):

    dfm_select = perid_above2[['min $DockQ_{i}$','MMscore','pTM', 'ipTM', 'min pDockQ', 'min $pDockQ_{i}$', 'class']]	
    
    dfm_select.rename(columns = {'min $DockQ_{i}$':'$DockQ_{i}$', 'min pDockQ':'pDockQ', 'min $pDockQ_{i}$':'$pDockQ_{i}$'}, inplace = True)

    def corrfunc(x, y, ax=None, **kws):
         r, _ = spearmanr(x, y)
         label = r'$\rho$='+ str(round(r, 3))
         ax = plt.gca()
         #pos = (.6, .1 - .1*n)
         ax.set_aspect('equal', adjustable='box')
         pos = (0.05,0.87) if kws['label'] == 'homomer' else (0.05,0.95)
         ax.annotate("{}:{}".format(kws['label'],label), xy = pos, size = 12, xycoords = ax.transAxes)
    
    g=sns.pairplot(data=dfm_select, hue='class', corner=True)
    g.map_lower(corrfunc)

    axis_font = {'size':'12'}

    plt.subplots_adjust(wspace = 0.2)
    g.set(ylim=(0,1.2))
    g.set(xlim=(0,1))
    plt.yticks(fontsize=12)
    plt.xticks(fontsize=12)
    handles = g._legend_data.values()
    labels = g._legend_data.keys()
    g.fig.legend(handles=handles, labels=labels, bbox_to_anchor=(0.85, 0.5))
    g._legend.remove()

    plt.savefig('figures/pairplot_2.jpg',format='jpg',dpi=450)

def corum(corum_id):
    sns.kdeplot(x='$pDockQ_{i}$', y='pTM', data=corum_id)
    plt.savefig('figures/corum_kde.jpg', format='jpg', dpi=350)


def supplementary_2():
    neff=pd.read_csv('../../data/csv/neff.csv',delimiter=' ', names=['pdbid', 'neff'])
    neff['neff_log'] = np.log(neff['neff']+0.001)

    peridm=perid.merge(neff, on=['pdbid'])
    #peridm=peridm[peridm['num_chains']>2]
    d1=peridm.loc[(peridm['min $DockQ_{i}$']<0.23)&(peridm['MMscore']<0.75)]#&(peridm['class']=='heteromer')]
    d2=peridm.loc[(peridm['min $DockQ_{i}$']>=0.23)&(peridm['MMscore']>=0.75)]#&(peridm['class']=='heteromer')]

    d11=d1[['pdbid', 'min $DockQ_{i}$', 'MMscore','neff_log', 'class']]
    d11['Classification']='Bad models'
    d22=d2[['pdbid', 'min $DockQ_{i}$', 'MMscore','neff_log', 'class']]
    d22['Classification']='Good models'

    dout=d11.append(d22)

    sns.boxplot(x="Classification", y="neff_log", hue='class', data=dout,palette='Set2') #inner="points"

    print (mannwhitneyu(dout.loc[(dout['Classification']=='Bad models') & (dout['class']=='homomer') ]['neff_log'],(dout.loc[(dout['Classification']=='Good models') & (dout['class']=='homomer')]['neff_log'])))

    axis_font = {'size':'12'}
    plt.ylabel("Log(Neff)",**axis_font)
    #plt.xlim(xmin=0)
    #plt.tight_layout()
    plt.legend(loc=4)
    plt.savefig('figures/neff.png', dpi=350)
    plt.close()

    perchain_top = pd.read_csv('../../output/scores_perchain_1mod.csv')
    perid_plot = pd.read_csv('../../output/perid_plot.csv')

    seqlen_diff=perchain_top.groupby('pdbid').apply(lambda grp:grp['seqlen'].max()-grp['seqlen'].min()).reset_index()
    seqlen_diff.rename(columns={0:'diff_seqlen'},inplace=True)
    perid_plot=perid_plot.merge(seqlen_diff,on=['pdbid'])

    perid_plot.rename(columns = {'min_miDockQ':'min $DockQ_{i}$', 'max_miDockQ':'max $DockQ_{i}$','avg_miDockQ':'avg $DockQ_{i}$', 
                        'min_pwDockQ':'min $DockQ_{ij}$', 'max_pwDockQ':'max $DockQ_{ij}$','avg_pwDockQ':'avg $DockQ_{ij}$',
                        'min_pDockQ':'min pDockQ','min_pmiDockQ':'$pDockQ_{i}$' }, inplace = True)

    perid_plot=perid_plot[perid_plot['num_chains']>2]

    peridm=perid_plot
    d1=peridm.loc[(peridm['min $DockQ_{i}$']<0.23)&(peridm['MMscore']<0.75)&(peridm['class']=='heteromer')]
    d2=peridm.loc[(peridm['min $DockQ_{i}$']>=0.23)&(peridm['MMscore']>=0.75)&(peridm['class']=='heteromer')]

    d11=d1[['pdbid', 'min $DockQ_{i}$', 'MMscore','diff_seqlen', 'class']]
    d11['Classification']='Bad models'
    d22=d2[['pdbid', 'min $DockQ_{i}$', 'MMscore','diff_seqlen', 'class']]
    d22['Classification']='Good models'

    dout=d11.append(d22)
    print (mannwhitneyu(dout.loc[(dout['Classification']=='Bad models')]['diff_seqlen'],(dout.loc[(dout['Classification']=='Good models')]['diff_seqlen'])))

    sns.boxplot(x="Classification", y="diff_seqlen", data=dout,palette='Set2') #inner="points"
    axis_font = {'size':'12'}
    plt.ylabel("$\Delta$ Chain lengths",**axis_font)
    plt.savefig('figures/delta_chainseq.png', dpi=350)
    plt.close()
    perchainm=perchain_top

    d1=perchainm.loc[(perchainm['DockQ']<0.23)]
    d2=perchainm.loc[(perchainm['DockQ']>=0.23)]

    d1['Classification']='Bad models'
    d2['Classification']='Good models'

    dout=d1.append(d2)

    sns.boxplot(x="Classification", y="diff_AA", hue='class', data=dout,palette='Set2')

    print (mannwhitneyu(dout.loc[(dout['Classification']=='Bad models') & (dout['class']=='homomer') ]['diff_AA'],(dout.loc[(dout['Classification']=='Good models') & (dout['class']=='homomer')]['diff_AA'])))

    #plt.savefig('figures/diffseqlen_seqres.png', dpi=350)

    axis_font = {'size':'12'}
    plt.ylabel("$\Delta$ SEQRES and PDB chain",**axis_font)
    #plt.xlim(xmin=0)
    #plt.tight_layout()
    plt.legend(loc=1)
    #plt.legend()
    plt.savefig('figures/delta_seqres.png', dpi=350)
    plt.close()

    perid_plot = pd.read_csv('../../output/perid_plot.csv')

    perid_plot.rename(columns = {'min_miDockQ':'min $DockQ_{i}$', 'max_miDockQ':'max $DockQ_{i}$','avg_miDockQ':'avg $DockQ_{i}$', 
                        'min_pwDockQ':'min $DockQ_{ij}$', 'max_pwDockQ':'max $DockQ_{ij}$','avg_pwDockQ':'avg $DockQ_{ij}$',
                        'min_pDockQ':'min pDockQ','min_pmiDockQ':'$pDockQ_{i}$' }, inplace = True)

    #perid_plot=perid_plot[perid_plot['num_chains']>2]

    peridm=perid_plot
    ds1 = perid_plot[perid_plot['Symmetry']=='C1']
    ds2 = perid_plot[perid_plot['Symmetry']!='C1']
    ds1['Symmetrical']='Symmetric'
    ds2['Symmetrical']='Asymmetric'

    dout=ds1.append(ds2)

    d1=dout.loc[(dout['min $DockQ_{i}$']<0.23)&(dout['MMscore']<0.75)]
    d2=dout.loc[(dout['min $DockQ_{i}$']>=0.23)&(dout['MMscore']>=0.75)]

    d1['Classification']='Bad models'
    d2['Classification']='Good models'

    dout2=d1.append(d2)

    print (mannwhitneyu(dout2.loc[(dout2['Classification']=='Bad models') & (dout2['class']=='heteromer') ]['Symmetrical'],(dout2.loc[(dout2['Classification']=='Good models') & (dout2['class']=='heteromer')]['Symmetrical'])))
    #print (mannwhitneyu(dout2.loc[(dout2['Classification']=='Bad models')]['Symmetrical'],(dout2.loc[(dout2['Classification']=='Good models')]['Symmetrical'])))

    sns.countplot(x='Classification', hue='Symmetrical', data=dout2)

    axis_font = {'size':'12'}
    plt.ylabel("Count",**axis_font)
    #plt.xlim(xmin=0)
    #plt.tight_layout()
    plt.legend(loc=2)
    plt.savefig('figures/symmetry.png', dpi=350)
    plt.close()


def supplementary_3():
    
    perid = pd.read_csv('../../output/perid_plot.csv')
    axis_font = {'size':'12'}
    all_petras_before=pd.read_csv('../../output/perid_v220.csv',delimiter=',',)
    petras_before = all_petras_before[['pdbid', 'ifTrain','class']].drop_duplicates()
    petras_before=petras_before[petras_before['ifTrain']=='bef']

    perid_before=pd.read_csv('../../output/DockQ_oldbiounit_perid.csv',delimiter=',', skiprows=1, names=['pdbid','model_num','DockQ'])
    perid_before_merged=perid_before.merge(petras_before,on=['pdbid']).drop_duplicates('pdbid')

    af = perid_before_merged.copy()

    af = af[['pdbid', 'ifTrain', 'DockQ', 'class']]
    af['class'] = af['class'].replace('hetero', 'heteromer')
    af['class'] = af['class'].replace('homo', 'homomer')

    pm=perid[['pdbid', 'min_miDockQ', 'class']]
    pm1=pm.copy()
    pm1['DockQ']=pm1['min_miDockQ']
    pm1['ifTrain']='After'
    pm1=pm1.drop(['min_miDockQ'], axis=1)

    dfmerge=pd.concat([af, pm1], ignore_index=True, sort=True)

    ## homo ttest 
    print (mannwhitneyu(dfmerge.loc[(dfmerge['ifTrain']!='After')&(dfmerge['class']=='homomer')]['DockQ'], dfmerge.loc[(dfmerge['ifTrain']=='After')&(dfmerge['class']=='homomer')]['DockQ']))
    # result: MannwhitneyuResult(statistic=428742.0, pvalue=1.4658493135330627e-57)
    ## hetero ttest
    print (mannwhitneyu(dfmerge.loc[(dfmerge['ifTrain']!='After')&(dfmerge['class']=='heteromer')]['DockQ'], dfmerge.loc[(dfmerge['ifTrain']=='After')&(dfmerge['class']=='heteromer')]['DockQ']))
    # result: MannwhitneyuResult(statistic=265595.0, pvalue=9.083933427554602e-17)

    print (mannwhitneyu(dfmerge.loc[(dfmerge['ifTrain']!='After')]['DockQ'], dfmerge.loc[(dfmerge['ifTrain']=='After')]['DockQ']))

    sns.kdeplot(dfmerge.loc[(dfmerge['ifTrain']!='After')&(dfmerge['class']=='homomer')]['DockQ'],color='brown',label='Homomers (homology to AFM training set)')
    sns.kdeplot(dfmerge.loc[(dfmerge['ifTrain']=='After')&(dfmerge['class']=='homomer')]['DockQ'],color='brown',linestyle="--",label='Homomers (no homology to AFM training set)')
    sns.kdeplot(dfmerge.loc[(dfmerge['ifTrain']!='After')&(dfmerge['class']=='heteromer')]['DockQ'],color='green',label='Heteromers (homology to AFM training set)')
    sns.kdeplot(dfmerge.loc[(dfmerge['ifTrain']=='After')&(dfmerge['class']=='heteromer')]['DockQ'],linestyle="--",color='green',label='Heteromers (no homology to AFM training set)')
    #plt.legend()
    plt.xlabel("$DockQ_{i}$",**axis_font)
    #plt.legend(bbox_to_anchor=(1.05, 0), loc='upper left', borderaxespad=0.)
    plt.legend()
    plt.savefig('figures/before_afterTraining.png', dpi=350)
    plt.close()

def corum(corum_id):
   corum_id[(corum_id['pTM']>.5) &(corum_id['$pDockQ_{i}$']>.23)]
   sns.kdeplot(x='$pDockQ_{i}$', y='pTM', data=corum_id)
   plt.savefig('figures/corum_kde.jpg', format='jpg', dpi=350)
   plt.close()



## Inputs
perid = pd.read_csv('../../output/perid_plot.csv')

perid.rename(columns = {'min_miDockQ':'min $DockQ_{i}$', 'max_miDockQ':'max $DockQ_{i}$','avg_miDockQ':'avg $DockQ_{i}$', 
                        'min_pwDockQ':'min $DockQ_{ij}$', 'max_pwDockQ':'max $DockQ_{ij}$','avg_pwDockQ':'avg $DockQ_{ij}$',
                        'min_pDockQ':'min pDockQ','min_pmiDockQ':'min $pDockQ_{i}$' }, inplace = True)

perchain = pd.read_csv('../../output/perchain_plot.csv')

perchain.rename(columns = {'miDockQ':'$DockQ_{i}$', 'pwDockQ':'$DockQ_{ij}$', 'pmiDockQ':'$pDockQ_{i}$' }, inplace = True)

perid_above2 = perid[perid['num_chains']>2]
perchain_above2 = perchain[perchain['num_chains']>2]

df=pd.read_csv('../../output/pDockQ_update.csv',delimiter=',')

corum_chain = pd.read_csv('../../output/corum_perchain.csv')

corum_id = pd.read_csv('../../output/corum_perid.csv')

corum_id.rename(columns = {'min_miDockQ':'min $DockQ_{i}$', 'max_miDockQ':'max $DockQ_{i}$','avg_miDockQ':'avg $DockQ_{i}$', 
                        'min_pwDockQ':'min $DockQ_{ij}$', 'max_pwDockQ':'max $DockQ_{ij}$','avg_pwDockQ':'avg $DockQ_{ij}$',
                        'min_pDockQ':'min pDockQ','min_pmiDockQ':'$pDockQ_{i}$' }, inplace = True)

perid_above2 = perid[perid['num_chains']>2]
perchain_above2 = perchain[perchain['num_chains']>2]

## Figure 1
#venn(perid_above2)

## Figure 2
#pairplot_1(perid_above2)
#successful_interfaces(perchain)

## Figure 3 
# from pymol session

## Figure 4
#pairplot_2(perid_above2)

## Figure 5
#corum(corum_id)

## Supplementary Figures 2
#supplementary_2()

## Supplementary Figures 3
#supplementary_3() 

## Corum
#corum(corum_id)


