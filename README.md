This repository contains all the data, scripts and analysis performed in the study **"Evaluation of AlphaFold-Multimer prediction on multi-chain protein complexes"**. 

1. Data \
Contains structural and sequence information for each oligomeric state \
	xmer 
	* pdb - biological unit files downloaded from PDB 
	* afm - AlphaFold-Multimer generated models
	* seqres - sequences obtained from the SEQRES record from PDB files

2.  Src \
Contains scripts for developing the homology reduced dataset and analyze the results
	* analyze (run_analysis.py generates all the figures in the manuscript)
	* pymol sessions 
	* figures

3. Output \
Contains the DockQ and MMscore scores calculated for the AFM models

