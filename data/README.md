Data folder contains the processed benchmark PDB-ids under each oligomeric state from 2-6mer

all_info.csv currently (04/10) contains the following headers (n=2054)
pdbid,Stucture Keywords,Oligomeric Count,Kind,Oligomeric State,Stoichiometry,Symbol,Type,num_chains,uniq_chains,class,diff_AA,resolution
where columns 2-7 are obtained from Custom Report created from RCSB PDB and 8-12 are obtained by parsing mmcif scripts 

Each folder contains:
	- ID_xmer_homo.csv (The homomeric complexes under this oligomeric state)
	- ID_xmer_hete.csv (The heteromeric complexes under this oligomeric state)
	- mmcif (The first biological assemblies belonging to this category)
	- seqres (The concatenated seqres sequence of the chains)
