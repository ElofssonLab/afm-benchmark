Analysis Type:	PANTHER Enrichment Test (release 20221017)
Annotation Version and Release Date:	GO Ontology database DOI:  10.5281/zenodo.6799722 Released 2022-07-01
Analyzed List:	all_5_heteromer_Dockq.csv (Homo sapiens)	There are duplicate IDs in the file.   For duplicates, the first id/value pair in the file will be used.
Correction:	NONE
GO biological process complete	number	overUnder	pvalue
establishment of localization in cell (GO:0051649)	5	-	3.30E-02
intracellular transport (GO:0046907)	5	-	3.30E-02
transport along microtubule (GO:0010970)	2	-	4.81E-02
regulation of proteolysis (GO:0030162)	2	-	4.81E-02
organelle transport along microtubule (GO:0072384)	2	-	4.81E-02
cytoskeleton-dependent intracellular transport (GO:0030705)	2	-	4.81E-02
establishment of mitochondrion localization (GO:0051654)	2	-	4.81E-02
mitochondrion localization (GO:0051646)	2	-	4.81E-02
mitochondrion transport along microtubule (GO:0047497)	2	-	4.81E-02
microtubule-based transport (GO:0099111)	2	-	4.81E-02
central nervous system neuron development (GO:0021954)	2	-	4.81E-02
central nervous system neuron differentiation (GO:0021953)	2	-	4.81E-02
microtubule-based movement (GO:0007018)	2	-	4.81E-02
regulation of mitochondrial membrane potential (GO:0051881)	2	-	4.81E-02
establishment of mitochondrion localization, microtubule-mediated (GO:0034643)	2	-	4.81E-02
