Analysis Type:	PANTHER Enrichment Test (release 20221017)
Annotation Version and Release Date:	GO Ontology database DOI:  10.5281/zenodo.6799722 Released 2022-07-01
Analyzed List:	all_4_heteromer_Dockq.csv (Homo sapiens)	There are duplicate IDs in the file.   For duplicates, the first id/value pair in the file will be used.
Correction:	NONE
GO biological process complete	number	overUnder	pvalue
amide biosynthetic process (GO:0043604)	5	+	1.19E-03
organic cyclic compound metabolic process (GO:1901360)	34	-	1.29E-03
nucleic acid metabolic process (GO:0090304)	26	-	1.51E-03
response to organic cyclic compound (GO:0014070)	13	-	2.08E-03
organelle localization by membrane tethering (GO:0140056)	4	+	2.48E-03
vesicle fusion (GO:0006906)	4	+	2.48E-03
organelle membrane fusion (GO:0090174)	4	+	2.48E-03
sphingolipid biosynthetic process (GO:0030148)	4	+	3.33E-03
membrane lipid biosynthetic process (GO:0046467)	4	+	3.33E-03
ceramide metabolic process (GO:0006672)	4	+	3.33E-03
sphingolipid metabolic process (GO:0006665)	4	+	3.33E-03
membrane lipid metabolic process (GO:0006643)	4	+	3.33E-03
mRNA processing (GO:0006397)	5	-	4.14E-03
mRNA metabolic process (GO:0016071)	5	-	4.14E-03
RNA splicing (GO:0008380)	5	-	4.14E-03
nucleobase-containing compound metabolic process (GO:0006139)	27	-	4.33E-03
diol biosynthetic process (GO:0034312)	3	+	6.14E-03
diol metabolic process (GO:0034311)	3	+	6.14E-03
polyol biosynthetic process (GO:0046173)	3	+	6.14E-03
sphingoid biosynthetic process (GO:0046520)	3	+	6.14E-03
sphingoid metabolic process (GO:0046519)	3	+	6.14E-03
ceramide biosynthetic process (GO:0046513)	3	+	6.14E-03
sphingosine biosynthetic process (GO:0046512)	3	+	6.14E-03
polyol metabolic process (GO:0019751)	3	+	6.14E-03
sphingosine metabolic process (GO:0006670)	3	+	6.14E-03
cellular aromatic compound metabolic process (GO:0006725)	30	-	6.68E-03
positive regulation of intracellular signal transduction (GO:1902533)	20	-	8.19E-03
establishment of protein localization to plasma membrane (GO:0061951)	4	+	9.86E-03
autophagosome membrane docking (GO:0016240)	3	+	1.03E-02
membrane docking (GO:0022406)	5	+	1.03E-02
cellular response to organic cyclic compound (GO:0071407)	9	-	1.06E-02
cellular amide metabolic process (GO:0043603)	10	+	1.12E-02
organelle fusion (GO:0048284)	5	+	1.27E-02
cellular response to growth factor stimulus (GO:0071363)	8	-	1.27E-02
response to growth factor (GO:0070848)	8	-	1.27E-02
positive regulation of nucleobase-containing compound metabolic process (GO:0045935)	19	-	1.30E-02
heterocycle metabolic process (GO:0046483)	29	-	1.33E-02
regulation of molecular function (GO:0065009)	45	-	1.38E-02
sphingomyelin biosynthetic process (GO:0006686)	3	+	1.42E-02
sphingomyelin metabolic process (GO:0006684)	3	+	1.42E-02
protein phosphorylation (GO:0006468)	7	-	1.58E-02
DNA metabolic process (GO:0006259)	17	-	1.61E-02
vesicle-mediated transport in synapse (GO:0099003)	3	+	1.62E-02
phosphorylation (GO:0016310)	8	-	1.68E-02
endocrine system development (GO:0035270)	4	-	1.85E-02
adrenal gland development (GO:0030325)	3	-	2.01E-02
hormone metabolic process (GO:0042445)	2	-	2.06E-02
tube morphogenesis (GO:0035239)	10	-	2.16E-02
regulation of cell differentiation (GO:0045595)	22	-	2.23E-02
regulation of hormone levels (GO:0010817)	8	-	2.24E-02
locomotion (GO:0040011)	6	-	2.25E-02
vesicle-mediated transport to the plasma membrane (GO:0098876)	4	+	2.25E-02
MAPK cascade (GO:0000165)	6	-	2.27E-02
epithelial tube morphogenesis (GO:0060562)	5	-	2.29E-02
regulation of phosphorus metabolic process (GO:0051174)	25	-	2.36E-02
regulation of phosphate metabolic process (GO:0019220)	25	-	2.36E-02
positive regulation of phosphorylation (GO:0042327)	13	-	2.39E-02
positive regulation of cell motility (GO:2000147)	7	-	2.43E-02
positive regulation of phosphorus metabolic process (GO:0010562)	14	-	2.46E-02
positive regulation of phosphate metabolic process (GO:0045937)	14	-	2.46E-02
body fluid secretion (GO:0007589)	2	+	2.49E-02
secretion by tissue (GO:0032941)	2	+	2.49E-02
positive regulation of DNA metabolic process (GO:0051054)	9	-	2.50E-02
regulation of hormone metabolic process (GO:0032350)	2	-	2.67E-02
toll-like receptor 7 signaling pathway (GO:0034154)	2	-	2.72E-02
toll-like receptor signaling pathway (GO:0002224)	2	-	2.72E-02
pattern recognition receptor signaling pathway (GO:0002221)	2	-	2.72E-02
regulation of phosphorylation (GO:0042325)	22	-	2.75E-02
sphinganine biosynthetic process (GO:0046511)	2	+	2.77E-02
sphinganine metabolic process (GO:0006667)	2	+	2.77E-02
positive regulation of lipophagy (GO:1904504)	2	+	2.77E-02
regulation of lipophagy (GO:1904502)	2	+	2.77E-02
membrane fusion (GO:0061025)	7	+	2.78E-02
vesicle organization (GO:0016050)	6	+	2.80E-02
exocytic process (GO:0140029)	2	+	2.82E-02
vesicle docking (GO:0048278)	2	+	2.82E-02
intracellular signal transduction (GO:0035556)	25	-	2.84E-02
organelle localization (GO:0051640)	14	+	2.87E-02
RNA processing (GO:0006396)	8	-	2.89E-02
regulation of ubiquitin-dependent protein catabolic process (GO:2000058)	3	-	2.97E-02
regulation of proteasomal ubiquitin-dependent protein catabolic process (GO:0032434)	3	-	2.97E-02
regulation of catalytic activity (GO:0050790)	33	-	2.99E-02
RNA metabolic process (GO:0016070)	12	-	3.04E-02
positive regulation of cellular metabolic process (GO:0031325)	43	-	3.05E-02
positive regulation of transferase activity (GO:0051347)	10	-	3.14E-02
taxis (GO:0042330)	5	-	3.15E-02
chemotaxis (GO:0006935)	5	-	3.15E-02
positive regulation of cellular process (GO:0048522)	76	-	3.27E-02
endosomal transport (GO:0016197)	3	+	3.39E-02
endocytic recycling (GO:0032456)	3	+	3.39E-02
positive regulation of cellular biosynthetic process (GO:0031328)	19	-	3.42E-02
positive regulation of biosynthetic process (GO:0009891)	19	-	3.42E-02
DNA repair (GO:0006281)	12	-	3.47E-02
positive regulation of cellular component biogenesis (GO:0044089)	7	-	3.51E-02
reactive oxygen species metabolic process (GO:0072593)	4	-	3.51E-02
cell motility (GO:0048870)	11	-	3.53E-02
cell migration (GO:0016477)	11	-	3.53E-02
respiratory system development (GO:0060541)	4	-	3.56E-02
lung development (GO:0030324)	4	-	3.56E-02
respiratory tube development (GO:0030323)	4	-	3.56E-02
response to cAMP (GO:0051591)	2	-	3.62E-02
negative regulation of hydrolase activity (GO:0051346)	6	-	3.69E-02
G protein-coupled receptor signaling pathway (GO:0007186)	10	-	3.75E-02
regulation of nucleobase-containing compound metabolic process (GO:0019219)	32	-	3.76E-02
chromosome separation (GO:0051304)	2	-	3.81E-02
positive regulation of lymphocyte mediated immunity (GO:0002708)	7	-	3.99E-02
positive regulation of leukocyte mediated immunity (GO:0002705)	7	-	3.99E-02
tube development (GO:0035295)	13	-	4.04E-02
regulation of autophagy (GO:0010506)	14	+	4.06E-02
positive regulation of transcription by RNA polymerase II (GO:0045944)	6	-	4.07E-02
positive regulation of autophagy (GO:0010508)	8	+	4.36E-02
tissue morphogenesis (GO:0048729)	10	-	4.42E-02
vesicle-mediated transport (GO:0016192)	16	+	4.50E-02
regulation of biosynthetic process (GO:0009889)	32	-	4.55E-02
embryo development (GO:0009790)	9	-	4.57E-02
positive regulation of locomotion (GO:0040017)	9	-	4.57E-02
regulation of proteolysis (GO:0030162)	10	-	4.66E-02
N-terminal protein amino acid acetylation (GO:0006474)	3	+	4.67E-02
protein acetylation (GO:0006473)	3	+	4.67E-02
protein acylation (GO:0043543)	3	+	4.67E-02
N-terminal protein amino acid modification (GO:0031365)	3	+	4.67E-02
positive regulation of nitrogen compound metabolic process (GO:0051173)	42	-	4.71E-02
establishment of protein localization to membrane (GO:0090150)	5	+	4.86E-02
positive regulation of protein-containing complex assembly (GO:0031334)	4	-	4.98E-02
